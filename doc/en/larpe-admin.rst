=====================================
Larpe - Administrator Guide
=====================================

:author: Damien Laniel
:contact: dlaniel@entrouvert.com
:copyright: Copyright © 2006 Entr'ouvert

.. contents:: Table of contents

Overview
========

Larpe is a Liberty Alliance Reverse Proxy. It allows any service provider
(that is a website) to use Liberty Alliance features (Identity federation,
Single Sign On and Single Logout) without changing the code of
the service provider itself. It uses the Lasso_ library
which is certified by the `Liberty Alliance`_ consortium. Lasso_ and Larpe
are released under the terms of the `GNU GPL license`_.


How to get and install Larpe
============================

Installation under Debian_ Sarge
++++++++++++++++++++++++++++++++

To work correctly Larpe relies on :

* Apache2_ ;

* Lasso_ (0.6.3) ;

* Quixote_ (2.0) ;

* SCGI_ ;

* mod_python_ ;

* libxml2 ;

* mod_proxy_html.

You will also need a Liberty Alliance Identity Provider, be it on the same server or not.
We recommend Authentic_ for that need.

Package Installation
--------------------

You need to add the following line to your /etc/apt/sources.list; this will
give you access to the repository where Larpe is stored::

 deb http://deb.entrouvert.org/ sarge main

As root type::

 apt-get update
 apt-get install larpe

And follow the debconf wizard to set it up.

All the required packages are now installed and configured.

You might need to change the "<VirtualHost \*>" in your apache2 configuration
(/etc/apache2/sites-available/apache2-vhost-larpe) depending on how you
previously configured apache.

Don't forget to modify your /etc/hosts file if necessary. Larpe now works, the
administration interface is reachable at http://your_domain_name/admin. The username
and password are the ones you entered during the installation wizard.

If you don't want to modify your sources.list file, you can manually dowload and
install the required packages with the dpkg -i command :

* Larpe, Authentic and Lasso on http://deb.entrouvert.org/ ;

* Quixote 2.0 on http://authentic.labs.libre-entreprise.org/.

Installation with another Linux distribution
++++++++++++++++++++++++++++++++++++++++++++

We suppose Apache2_, SCGI_, mod_python_, libxml2 and mod_proxy_html are already installed. You need then to
download and install the following sources :

* Lasso http://lasso.entrouvert.org ;

* Quixote http://www.mems-exchange.org/software/Quixote/ ;

* Authentic http://authentic.labs.libre-entreprise.org/ ;

* Larpe http://labs.libre-entreprise.org/frs/?group_id=108.

To install Larpe, uncompress the sources you have downloaded and launch the
setup.py script ::

 tar xzf larpe*.tar.gz
 cd larpe*
 python setup.py install

You need then to configure Apache2_ correctly. You should use the provided apache2-vhost-larpe template and adapt to your configuration.

Don't forget to modify your /etc/hosts file if necessary. Larpe now works, the
administration interface is reachable at http://your_domain_name/admin.

Basic Larpe configuration
=========================

Identity Provider configuration
+++++++++++++++++++++++++++++++

If you don't have a configured Identity Provider yet, please read Authentic
manual to set it up. Then you must have the metadata and public key of the Identity
Provider to begin with Larpe.

Then in Larpe administration interface, click on "Settings", then "Identity Provider".
Fill in the metadata and public key that you've got from your Identity Provider then
click Submit.
Your Identity Provider is now configured in Larpe, you can then configure as many Service
Providers as you want.

Service Provider Configuration
++++++++++++++++++++++++++++++

Service Provider configuration
------------------------------

Click on "Hosts" then "New Host".

Fill in the following parameters :

* Label : the name you want to give to your Service Provider ;

* Original Site Address : the root URL of your Service Provider ;

* Authentication Page : if the page which contains the authentication form for
  your Service Provider is on a separate page, fill the url of this page here ;

* Authentication Form Page : if you didn't fill the previous field and if the
  authentication form if not on the first page of your Service Provider either,
  fill the url of the page which contains the authentication form here ;

* Logout Address : when you want Single Sign On and Identity Federation, you probably
  want Single Logout too. If so, fill the logout url of your original site here ;

* Reversed Host Name : the domain name where you want to access your Service Provider
  through the reverse proxy. It can be the domain name of Larpe or not ;

Then click "Submit". Wait a few seconds then go to http://reversed_host_name/reverse_directory/
to check if it works. If not, wait a bit more and try again. If it really doesn't work,
please submit a bug report at http://labs.libre-entreprise.org/tracker/?func=add&group_id=108&atid=512

Service Provider Example: Linuxfr
---------------------------------

To help you setup your own Service Provider, we provide an example of a working Service Provider
to guide you.

To setup Linuxfr, fill in the following parameters :

* Label : Linuxfr ;

* Original Site Address : http://linuxfr.org/ ;

* Authentication Page : Nothing here ;

* Authentication Form Page : http://linuxfr.org/pub/ ;

* Logout Address : http://linuxfr.org/close_session.html ;

* Reversed Host Name : linuxfr.reverse-proxy.example.com.

With "reverse-proxy.example.com" being the hostname you've set up before for your reverse-proxy

Don't forget to add this new hostname to your /etc/hosts as well.

You can then go to the reversed Linuxfr at http://linuxfr.reverse-proxy.example.com/

Service Provider Liberty Alliance final setup
---------------------------------------------

Now that you can access your Service Provider, you need a final step to use Liberty Alliance
features. Click on "Hosts", the click on the "Edit" icon of the Service Provider you've
just configured. Save the Service Provider Metadata (for ID-FF 1.2) and the Public Key
(right click then "Save as"). Configure this Service Provider on your Identity Provider
with these two files.

Licenses
========

Larpe, Authentic_, Candle_ and Lasso_ are released under the terms of the
`GNU GPL license`_.

.. _Lasso: http://lasso.entrouvert.org/
.. _`Liberty Alliance`: http://projectliberty.org/
.. _`GNU GPL License`: http://www.gnu.org/copyleft/gpl.html
.. _Debian: http://www.debian.org/
.. _Apache2: http://httpd.apache.org/
.. _Quixote: http://www.mems-exchange.org/software/Quixote
.. _mod_python: http://www.modpython.org/
.. _SCGI: http://www.mems-exchange.org/software/scgi/
.. _Candle: http://candle.labs.libre-entreprise.org/
.. _Authentic: http://www.entrouvert.com/fr/authentic/
