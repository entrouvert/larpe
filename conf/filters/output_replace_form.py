import re

def filter_page(filter, page):
    current_form = re.compile('<form [^>]*?action="%(auth_form_action)s".*?>.*?</form>', re.DOTALL)
    page = current_form.sub('<form method="post" action="/liberty/%(name)s/login"><input type="submit" value="Connexion" /></form>', page)
    return page

def outputfilter(filter):
    # Only filter html code
    if filter.req.content_type is not None:
        is_html = re.search('text/html', filter.req.content_type)
    if filter.req.content_type is None or not is_html:
        filter.pass_on()
    else:
        if not hasattr(filter.req, 'temp_doc'):
            # Create a new attribute to hold the document
            filter.req.temp_doc = []
            # If content-length ended up wrong, Gecko browsers truncated data
            if 'Content-Length' in filter.req.headers_out:
                del filter.req.headers_out['Content-Length']

        temp_doc = filter.req.temp_doc
        s = filter.read()
        # Could get '' at any point, but only get None at end
        while s:
            temp_doc.append(s)
            s = filter.read()

        # The end
        if s is None:
            page = ''.join(temp_doc)
            page = filter_page(filter, page)
            filter.write(page)
            filter.close()

