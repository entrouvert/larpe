%{!?python_sitearch: %define python_sitearch %(%{__python} -c 'from distutils import sysconfig; print sysconfig.get_python_lib(1)')}
# eval to 2.3 if python isn't yet present, workaround for no python in fc4 minimal buildroot
%{!?python_version: %define python_version %(%{__python} -c 'import sys; print sys.version.split(" ")[0]' || echo "2.3")}
%define apacheconfdir   %{_sysconfdir}/httpd/conf.d

Summary: Liberty Alliance Reverse Proxy
Name: larpe
Version: 0.2.9
Release: 2%{?dist}
License: GPL
Group: System Environment/Applications
Url: http://larpe.labs.libre-entreprise.org/
Source0: http://labs.libre-entreprise.org/frs/download.php/591/%{name}-%{version}.tar.gz
Buildroot: %{_tmppath}/%{name}-%{version}-%(id -u -n)
BuildRequires: python >= 2.3, python-quixote >= 2.0
BuildRequires: gettext
Requires: httpd >= 2.0, mod_scgi, mod_proxy_html
Requires: lasso-python >= 0.6.3, python-quixote >= 2.0, python-scgi
Requires: initscripts
Requires(post): /sbin/chkconfig
Requires(preun):/sbin/chkconfig
Requires(preun): /sbin/service

%description
Larpe is a Liberty Alliance Reverse Proxy. It allows any service provider (that is a website)
to use Liberty Alliance features (Identity federation, Single Sign On and Single Logout) without
changing the code of the service provider itself.

It uses the Lasso library which is certified by the Liberty Alliance consortium.

It is a quixote application and is commonly runned inside Apache web server.

%package doc
Summary: Documentation files for %{name} development.
Group: Documentation
BuildRequires: python-docutils, tetex-latex

%description doc
This package contains development documentation for %{name}.

%prep
%setup -q

# Change Apache vhost path in Larpe config
sed -i s#"/var/log/apache2/larpe-access.log"#"logs/larpe_access_log combined\n    TransferLog logs/larpe_access_log"# conf/apache2-vhost-larpe
sed -i s#"/var/log/apache2/larpe-error.log"#"logs/larpe_error_log"# conf/apache2-vhost-larpe
sed -i s#"APACHE_MAIN_VHOST.*$"#"APACHE_MAIN_VHOST='/etc/httpd/conf.d/larpe.conf'"# larpe/Defaults.py

%build

%install
rm -rf %{buildroot}

# install generic files
make install prefix=%{_prefix} DESTDIR=%{buildroot}

# install init script
install -d %{buildroot}/%{_initrddir}
install -p -m 0755 fedora/larpe.init %{buildroot}%{_initrddir}/larpe

# apache configuration
mkdir -p %{buildroot}%{apacheconfdir}
install -p -m 644 conf/apache2-vhost-larpe %{buildroot}%{apacheconfdir}/larpe.conf

# apache reload script
install -p -m 0755 fedora/larpe-reload-apache2-script %{buildroot}%{_sbindir}/

# install doc files
install -d -m 0755 %{buildroot}%{_datadir}/gtk-doc/html/larpe
make -C doc DESTDIR=%{buildroot}%{_datadir}/gtk-doc/html/larpe

%clean
rm -fr %{buildroot}

%post
/sbin/chkconfig --add %{name}

# manual post-installation
cat <<_EOF_
You must edit first %{apacheconfdir}/larpe.conf

You must enable Larpe with "chkconfig larpe on ; service larpe start"

You must also restart Apache with "service httpd restart"!
_EOF_

%preun
if [ $1 = 0 ]; then
	/sbin/service %{name} stop > /dev/null 2>&1
	/sbin/chkconfig --del %{name}
fi

%files
%defattr(-,root,root,755)
%config %{_initrddir}/larpe
%config(noreplace) %{apacheconfdir}/larpe.conf
%config(noreplace) %{_sysconfdir}/larpe/apache2-vhost-larpe-common
%{_sbindir}/larpectl
%{_sbindir}/larpe-reload-apache2
%{_sbindir}/larpe-reload-apache2-script
%{python_sitearch}/%{name}
%{_datadir}/%{name}
%{_datadir}/locale/fr/LC_MESSAGES/larpe.mo
/var/lib/larpe
%defattr(644,root,root,755)
%doc AUTHORS COPYING NEWS README

%files doc
%defattr(-,root,root)
%doc %{_datadir}/gtk-doc/html/%{name}

%changelog
* Tue Mar 05 2009 Jean-Marc Liger <jmliger@siris.sorbonne.fr> 0.2.9-2
- Added missing BuildRequires gettext
- Enabled larpe init script

* Mon Jan 19 2009 Damien Laniel <dlaniel@entrouvert.com> 0.2.9-1
- Updated to 0.2.9
- Use Larpe Makefile to install generic files
- Copy fedora specific files

* Sat Jan 17 2009 Jean-Marc Liger <jmliger@siris.sorbonne.fr> 0.2.1-2
- Added missing BuildRequires tetex-latex for doc subpackage
- Rebuilt on CentOS 4,5

* Wed Jan 14 2009 Jean-Marc Liger <jmliger@siris.sorbonne.fr> 0.2.1-1
- Updated to 0.2.1
- Added missing Requires lasso-python
- Added missing Requires python-scgi
- Rebuilt on CentOS 4,5

* Fri Mar 02 2007 Jean-Marc Liger <jmliger@siris.sorbonne.fr> 0.2.0-1
- Updated to 0.2.0
- Added BuildRequires python-quixote
- Built on Fedora Core 3 / RHEL 4 and Fedora Core 6 / RHEL 5

* Wed Jan 24 2007 Jean-Marc Liger <jmliger@siris.sorbonne.fr> 0.1.0-1
- First 0.1.0
- Built on Fedora Core 3 / RHEL 4 and Fedora Core 6 / RHEL 5
