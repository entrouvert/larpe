'''Default global variables'''

APP_DIR = '/var/lib/larpe'
DATA_DIR = '/usr/share/larpe'
ERROR_LOG = None #'/var/log/larpe.log'
WEB_ROOT = 'larpe'
APACHE_MAIN_VHOST = '/etc/apache2/sites-available/apache2-vhost-larpe'
APACHE_VHOST_COMMON = '/etc/larpe/apache2-vhost-larpe-common'
APACHE_RELOAD = '/usr/sbin/larpe-reload-apache2'
