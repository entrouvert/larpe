'''User object. Configuration variables and utilities'''

from qommon.storage import StorableObject

class User(StorableObject):
    '''User object. Configuration variables and utilities'''
    _names = 'users'

    name = None
    email = None
    name_identifiers = None
    identification_token = None
    lasso_dumps = None
    is_admin = False
    anonymous = False

    def __init__(self, name=None):
        StorableObject.__init__(self)
        self.name = name
        self.name_identifiers = []
        self.lasso_dumps = []

    def migrate(self):
        pass

    def remove_name_identifier(self, name_identifier):
        self.name_identifiers.remove(name_identifier)
        if not self.name_identifiers:
            self.remove_self()
        else:
            self.store()

    def get_display_name(self):
        if self.name:
            return self.name
        if self.email:
            return self.email
        return _('Unknown User')
    display_name = property(get_display_name)

    def __str__(self):
        return 'User %s, name : %s, name identifiers : %s, lasso_dumps : %s, token : %s' \
                % (self.id, self.name, self.name_identifiers, self.lasso_dumps,
                   self.identification_token)
