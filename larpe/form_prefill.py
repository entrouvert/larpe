from qommon.storage import StorableObject

class FormPrefill(StorableObject):
    _names = 'form_prefill'

    host_id = 0
    name = None
    url = None
    profile = None
    prefix = None
