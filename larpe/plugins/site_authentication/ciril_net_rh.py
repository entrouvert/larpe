
import re

from quixote import redirect

from larpe.plugins import site_authentication_plugins
from larpe.site_authentication import SiteAuthentication

class CirilSiteAuthentication(SiteAuthentication):

    plugin_name = 'ciril'
    output_filters = ['output_ciril_net_rh']

    def auto_detect_site(cls, html_doc):
        if re.search(
                """<form name="myForm" id="myForm" method="post" target="choixAppli" action="/cgi-bin/acces.exe" """,
                html_doc):
            return True
        return False
    auto_detect_site = classmethod(auto_detect_site)

    def check_auth(self, status, data):
        success = False
        return_content = ''

        # If status is 500, fail without checking other criterias
        if status // 100 == 5:
            success = False
            return_content = redirect(self.host.get_return_url())

        regexp = re.compile("""window.open\('/net_rh/accueil.php\?(.+?)'\,'_blank'""",
                re.DOTALL | re.IGNORECASE)
        m = regexp.search(data)
        if m:
            success = True
            link = "/net_rh/accueil.php?%s" % m.group(1)
            return_content = redirect(link)

        return success, return_content



site_authentication_plugins.register(CirilSiteAuthentication)

