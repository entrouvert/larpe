
class SiteAuthenticationPlugins:
    """ This class manages the plugins for site authentification """

    def __init__(self):
        self.site_authentication_classes = dict()

    def register(self, klass):
        """ Register a custom SiteAuthentification instance """
        self.site_authentication_classes[klass.plugin_name] = klass

    def get(self, plugin_name):
        """ Return a custom SiteAuthentification instance """
        if self.site_authentication_classes.has_key(plugin_name):
            return self.site_authentication_classes[plugin_name]
        else:
            return None

    def get_plugins_name(self):
        plugins_name = list()
        for plugin_name in self.site_authentication_classes.iterkeys():
            plugins_name.append(plugin_name)
        return plugins_name

    def auto_detect(self, html_doc):
        """
        Try to find automatically the right plugin name
        Return the plugin name or None
        """
        for name, klass in self.site_authentication_classes.iteritems():
            if klass.auto_detect_site(html_doc):
                return klass.plugin_name
        return None

