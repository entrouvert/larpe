'''Federation object. Configuration variables and utilities'''

from qommon.storage import StorableObject

class Federation(StorableObject):
    _names = 'federations'

    username = None
    password = None
    host_id = None
    name_identifiers = None
    cookies = None
    select_fields = {}

    def __init__(self, username, password, host_id, name_identifier, cookies=None, select=None):
        select = select or {}
        StorableObject.__init__(self)
        self.username = username
        self.password = password
        self.host_id = host_id
        self.name_identifiers = [ name_identifier ]
        self.cookies = cookies
        self.select_fields = select

    def remove_name_identifier(self, name_identifier):
        self.name_identifiers.remove(name_identifier)
        if not self.name_identifiers:
            self.remove_self()

    def set_cookies(self, cookies):
        self.cookies = cookies

    def __str__(self):
        return 'Federation username : %s, name identifiers : %s, cookies : %s' \
                % (self.username, self.name_identifiers, self.cookies)
