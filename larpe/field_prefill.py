from qommon.storage import StorableObject

class FieldPrefill(StorableObject):
    _names = 'field_prefill'

    form_id = 0
    name = None
    xpath = None
    number = 1
    raw_xml = False
    regexp_match = None
    regexp_replacing = None
    select_options = {}
