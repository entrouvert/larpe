'''Setup path and load Lasso library'''
try:
    from quixote.ptl import compile_package
    compile_package(__path__)
except ImportError:
    pass

import sys
import os
sys.path.insert(0, os.path.dirname(__file__))

import qommon

try:
    import lasso
except ImportError:
    lasso = None

if lasso and not hasattr(lasso, 'SAML2_SUPPORT'):
    lasso.SAML2_SUPPORT = False

