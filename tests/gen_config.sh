#!/bin/sh

if [ $# -eq 0 ];
then
    echo "Usage : $0 site_name"
    exit 1
fi

while [ $# -ne 0 ];
do
    if [ -d $1 ];
    then
        config_file="$1/config"
        echo -n "Generating config template for $1 ... "
        echo "setlocal sp_login_url " >> $config_file
        echo "setlocal sp_logout_url " >> $config_file
        echo "setlocal defederation_url " >> $config_file
        echo "setlocal idp_username " >> $config_file
        echo "setlocal idp_password " >> $config_file
        echo "setlocal sp_username " >> $config_file
        echo "setlocal sp_password " >> $config_file
        echo "setlocal find_username " >> $config_file
        echo "[OK]"
    fi
    shift
done
