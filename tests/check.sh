#!/bin/sh
#
# Usage : check.sh                : Test all sites in current directory
#         check.sh list_of_sites  : Test the sites whose name is given on the command line

exclude_sites="linuxfr listes_libre_entreprise egroupware"

if [ $# -eq 0 ];
then
    # Test all sites
    for site in *;
    do
        test_site=1
        for exclude_site in $exclude_sites;
        do
            if [ $site = $exclude_site ];
            then
                test_site=0
            fi
        done
        if [ $test_site -eq 1 ];
        then
            sites="$sites $site"
        fi
    done
else
    # Test the sites whose name is given on the command line
    while [ $# -ne 0 ];
    do
        sites="$sites $1"
        shift
    done
fi

function check() {
    site="$1"
    files=""

    # Combine as many functionnalities as given, at the same time
    shift
    echo -n "  "
    while [ $# -ne 0 ];
    do
        functionnality="$1"
        echo -n "$functionnality "
        case "$functionnality" in
            "Federation")
                add_files="sso idp_login federation $site/sso_check";;
            "SSO")
                add_files="sso idp_login $site/sso_check";;
            "SLO")
                add_files="slo $site/slo_check";;
        esac
        files="$files $add_files"
        shift
    done
    echo -n "..."

    cat $site/config $files | twill-sh > twill_output
    grep "ERROR" twill_output &> /dev/null
    if [ $? -eq 0 ];
    then
        echo "	[FAILED]"
        cat twill_output
        exit 1
    else
        echo "	[OK]"
    fi
}

echo
# Check loop
for site in $sites;
do
    if [ ! -d $site ];
    then
        continue
    fi

    echo "Testing $site :"

    echo -n "  Defederation ..."
    cat $site/config defederation idp_login | twill-sh &> /dev/null
    echo "	[OK]"
    check $site "Federation"
    check $site "SSO"
    check $site "SLO"
    check $site "SSO" "SLO"
    check $site "SLO" "SSO" "SLO" "SLO" "SSO" "SLO"

    rm -f twill_output
    echo
done
