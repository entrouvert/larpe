prefix = /usr
config_prefix = /var
config_dir = $(config_prefix)/lib/larpe

INSTALL = /usr/bin/install -c
PYTHON = /usr/bin/python

LARPE_VERSION = 0.2.9

larpe-reload-apache2: larpe-reload-apache2.c

install: larpe-reload-apache2
	rm -rf build
	$(MAKE) -C po install
	$(PYTHON) setup.py install --root "$(DESTDIR)/" --prefix "$(prefix)" --no-compile
	$(INSTALL) -d $(DESTDIR)$(prefix)/sbin/
	$(INSTALL) larpectl $(DESTDIR)$(prefix)/sbin/
	$(INSTALL) -m 4550 larpe-reload-apache2 $(DESTDIR)$(prefix)/sbin/
	$(INSTALL) -d $(DESTDIR)/etc/larpe
	$(INSTALL) -m 644 conf/apache2-vhost-larpe-common $(DESTDIR)/etc/larpe
	$(INSTALL) -d $(DESTDIR)$(config_dir)
	$(INSTALL) -d $(DESTDIR)$(config_dir)/vhosts.d
	$(INSTALL) -d $(DESTDIR)$(config_dir)/vhost-locations.d
	$(INSTALL) -d $(DESTDIR)$(config_dir)/vhosts.d.disabled
	$(INSTALL) -d $(DESTDIR)$(config_dir)/vhost-locations.d.disabled

uninstall:
	$(MAKE) -C po uninstall
	-rm -f $(DESTDIR)$(prefix)/sbin/larpe-reload-apache2
	-rm -f $(DESTDIR)$(prefix)/sbin/larpe-reload-apache2-script
	-rm -f $(DESTDIR)$(prefix)/sbin/larpectl
	-rm -rf $(DESTDIR)$(prefix)/share/larpe/
	@echo
	@echo "Depending on your Python version, you will have to remove manually the files in /usr/lib/python(version)/site-packages/larpe/"

clean:
	$(MAKE) -C po clean
	$(MAKE) -C doc clean
	-$(PYTHON) setup.py clean
	-rm -f larpe-reload-apache2

dist: clean
	tar czf dist/larpe-$(LARPE_VERSION).tar.gz -C .. --transform s/trunk/larpe-$(LARPE_VERSION)/ --exclude-from=exclude_from_dist trunk

.PHONY: clean dist
