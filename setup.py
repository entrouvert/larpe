#!/usr/bin/env python

import os
import distutils.core
from quixote.ptl.qx_distutils import qx_build_py

local_cfg = None
if os.path.exists('larpe/larpe_cfg.py'):
    local_cfg = open('larpe/larpe_cfg.py').read()
    os.unlink('larpe/larpe_cfg.py')

def data_tree(destdir, sourcedir):
    extensions = ['.css', '.png', '.jpeg', '.jpg', '.xml', '.html', '.js', '.py']
    r = []
    for root, dirs, files in os.walk(sourcedir):
        l = [os.path.join(root, x) for x in files if os.path.splitext(x)[1] in extensions]
        r.append( (root.replace(sourcedir, destdir, 1), l) )
        if '.svn' in dirs:
            dirs.remove('.svn')
    return r

distutils.core.setup(
        name = 'larpe',
        version = '1.1.1',
        maintainer = 'Jerome Schneider',
        maintainer_email = 'jschneider@entrouvert.com',
        url = 'http://larpe.labs.libre-entreprise.org',
        package_dir = { 'larpe': 'larpe' },
        packages = ['larpe', 'larpe.admin', 'larpe.ctl', 'larpe.plugins', 'larpe.plugins.site_authentication', 'larpe.qommon',
                    'larpe.qommon.admin', 'larpe.qommon.ident', 'larpe.qommon.backoffice', 'larpe.qommon.vendor'],
        cmdclass = {'build_py': qx_build_py},
        data_files = data_tree('share/larpe/web/', 'root/') + \
                     data_tree('share/larpe/web/larpe/qo/', 'larpe/qommon/static/') + \
                     data_tree('share/larpe/', 'conf/')
    )

if local_cfg:
    open('larpe/larpe_cfg.py', 'w').write(local_cfg)

