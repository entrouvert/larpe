#! /bin/sh
### BEGIN INIT INFO
# Provides:          larpe
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs $network
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start Larpe Liberty Alliance reverse proxy
# Description:       Start Larpe Liberty Alliance reverse proxy
### END INIT INFO

set -e

# Gracefully exit if the package has been removed.
test -x $DAEMON || exit 0

# Source function library
. /lib/lsb/init-functions

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
DESC="larpe"
NAME=larpe
DAEMON=/usr/sbin/larpectl
PIDFILE=/var/run/$NAME.pid
SCRIPTNAME=/etc/init.d/$NAME


# Read config file if it is present.
if [ -r /etc/default/$NAME ]
then
    . /etc/default/$NAME
fi

#
# Function that starts the daemon/service.
#
d_start() {
    start-stop-daemon --start --quiet --pidfile $PIDFILE --oknodo \
		--chuid www-data:www-data --make-pidfile --background --exec $DAEMON -- start $OPTIONS
}

#
# Function that stops the daemon/service.
#
d_stop() {
    start-stop-daemon --stop --quiet --pidfile $PIDFILE --oknodo
    rm -f $PIDFILE
}

case "$1" in
    start)
        log_begin_msg "Starting $DESC: $NAME"
        d_start
        log_end_msg $?
        ;;

    stop)
        log_begin_msg "Stopping $DESC: $NAME"
        d_stop
        log_end_msg $?
        ;;

    restart|force-reload)
    #
    #	If the "reload" option is implemented, move the "force-reload"
    #	option to the "reload" entry above. If not, "force-reload" is
    #	just the same as "restart".
    #
        log_begin_msg "Restarting $DESC: $NAME"
        d_stop
        sleep 1
        d_start
        log_end_msg $?
        ;;
    
    *)
        echo "Usage: $SCRIPTNAME {start|stop|restart|force-reload}" >&2
        exit 1
        ;;
esac

exit 0
