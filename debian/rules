#!/usr/bin/make -f
# GNU copyright 1997 to 1999 by Joey Hess.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

PYTHON=/usr/bin/python

LARPE_USER = www-data
LARPE_GROUP = www-data

ifneq (,$(findstring debug,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -g
endif
ifeq (,$(findstring nostrip,$(DEB_BUILD_OPTIONS)))
	INSTALL_PROGRAM += -s
endif

build: build-stamp

build-stamp:
	dh_testdir
	touch build-stamp

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp

	make clean

	dh_clean

install: build
	dh_testdir
	dh_testroot
	dh_clean -k
	dh_installdirs

	make install prefix=/usr DESTDIR=$(CURDIR)/debian/larpe
	# Apache Vhost
	dh_install conf/apache2-vhost-larpe etc/apache2/sites-available
	# Apache reload script
	dh_install debian/larpe-reload-apache2-script usr/sbin

	# Give files ownership to Larpe user and group
	chown -R $(LARPE_USER):$(LARPE_GROUP) $(CURDIR)/debian/larpe/usr/share/larpe/
	chown -R $(LARPE_USER):$(LARPE_GROUP) $(CURDIR)/debian/larpe/var/lib/larpe/
	chgrp $(LARPE_GROUP) $(CURDIR)/debian/larpe/usr/sbin/larpe-reload-apache2

# Build architecture-independent files here.
binary-indep: build install
# We have nothing to do by default.

# Build architecture-dependent files here.
binary-arch: build install
	dh_testdir
	dh_testroot
	dh_installdocs
	dh_installinit
	dh_installchangelogs
	dh_link
	dh_strip
	dh_compress
	dh_fixperms -X /var/lib/larpe -X /usr/sbin/larpe-reload-apache2
	dh_pysupport
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install
